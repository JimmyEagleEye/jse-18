package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.service.IUserService;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyLoginException;
import ru.korkmasov.tsc.exception.empty.EmptyPasswordException;
import ru.korkmasov.tsc.exception.system.AccessDeniedException;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.HashUtil;

public class AuthService implements ru.korkmasov.tsc.api.service.IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() throws EmptyIdException {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.add(login, password, email);
    }
}
