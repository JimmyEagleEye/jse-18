package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.IProjectTaskService;
import ru.korkmasov.tsc.api.ITaskRepository;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.api.IProjectRepository;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    public Project removeProjectById(final String userId, final String projectId) throws ProjectNotFoundException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @Override
    public Project removeProjectById(String projectId) {
        return null;
    }

    @Override
    public List<Task> findTaskByProjectId(final String userId, final String projectId) throws ProjectNotFoundException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskById(final String userId, final String taskId, final String projectId) throws TaskNotFoundException, ProjectNotFoundException {
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        final Task task = taskRepository.findById(userId, taskId); //
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(userId, taskId, projectId);
    }

    @Override
    public Task unbindTaskById(final String userId, final String taskId) throws TaskNotFoundException {
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        final Task task = taskRepository.findById(userId, taskId); //
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) return null;
        String projectId = projectRepository.findByIndex(userId, index).getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @Override
    public Project removeProjectByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) return null;
        String projectId = projectRepository.findByName(userId, name).getId();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

}
