package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.ITaskRepository;
import ru.korkmasov.tsc.api.ITaskService;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;

import java.util.List;
import java.util.Comparator;
import java.util.Date;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final String userId) {
        return taskRepository.findAll(userId);
    }

    /*@Override
    public Task add(final String name, final String description) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }*/

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(userId, comparator);
    }

    @Override
    public Task findById(final String userId, final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) throws EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public Task add(String name, String description) {
        return null;
    }

    @Override
    public Task findByName(final String userId, final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task add(final String userId, final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public void remove(final String userId, final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(userId, task);
    }

    @Override
    public Task removeById(final String userId, final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(userId, id);
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) throws EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task removeByName(final String userId, final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @Override
    public void clear(final String userId) {
        taskRepository.clear(userId);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) throws EmptyIdException, EmptyNameException, TaskNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) throws EmptyNameException, EmptyIndexException, TaskNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(final String userId, final String id) throws EmptyIdException, TaskNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) throws EmptyIndexException, TaskNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(final String userId, final String name) throws TaskNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) throws EmptyIdException, TaskNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) throws TaskNotFoundException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) throws TaskNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

}
