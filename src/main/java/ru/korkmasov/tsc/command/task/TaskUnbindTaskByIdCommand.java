package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class TaskUnbindTaskByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-unbind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() throws EmptyIdException, TaskNotFoundException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter task id");
        String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(user.getId(), taskId);
        if (task == null) throw new TaskNotFoundException();
        final Task taskUpdated = serviceLocator.getProjectTaskService().unbindTaskById(user.getId(), taskId);
        if (taskUpdated == null) incorrectValue();
    }
}
