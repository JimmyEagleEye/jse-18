package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class TaskUpdateByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws EmptyIdException, TaskNotFoundException, EmptyNameException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(user.getId(), id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateById(user.getId(), id, name, description);
        if (taskUpdated == null) incorrectValue();
    }
}

