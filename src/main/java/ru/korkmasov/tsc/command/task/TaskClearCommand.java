package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.User;

public class TaskClearCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Override
    public void execute() throws EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        serviceLocator.getTaskService().clear(user.getId());
    }
}
