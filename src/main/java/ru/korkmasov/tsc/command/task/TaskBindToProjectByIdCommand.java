package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class TaskBindToProjectByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-bind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() throws TaskNotFoundException, ProjectNotFoundException, EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter task id");
        String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(user.getId(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        String projectId = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getProjectTaskService().bindTaskById(user.getId(), taskId, projectId);
        if (taskUpdated == null) incorrectValue();
    }
}

