package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-find-by-project-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all task in project by project id.";
    }

    @Override
    public void execute() throws ProjectNotFoundException, EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        String id = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(((User) user).getId(), id);
        System.out.println("Task list for project");
        for (Task task : tasks) {
            System.out.println(task.toString());
        }
    }
}

