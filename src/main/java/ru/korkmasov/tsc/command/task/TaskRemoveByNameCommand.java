package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

public class TaskRemoveByNameCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() throws EmptyNameException, TaskNotFoundException, EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter name");
        String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeByName(user.getId(), name);
        if (task == null) throw new TaskNotFoundException();
    }
}

