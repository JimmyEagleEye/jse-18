package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

public class TaskFinishByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by id.";
    }

    @Override
    public void execute() throws EmptyIdException, TaskNotFoundException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishById(user.getId(), id);
        if (task == null) throw new TaskNotFoundException();
    }
}

