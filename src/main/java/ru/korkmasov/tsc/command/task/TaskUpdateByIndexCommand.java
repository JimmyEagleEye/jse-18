package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.command.TaskAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class TaskUpdateByIndexCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() throws IndexIncorrectException, EmptyIndexException, TaskNotFoundException, EmptyNameException, EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findByIndex(user.getId(), index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateByIndex(user.getId(), index, name, description);
        if (taskUpdated == null) incorrectValue();
    }
}

