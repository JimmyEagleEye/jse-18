package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.command.ProjectAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class ProjectUpdateByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() throws EmptyIdException, ProjectNotFoundException, EmptyNameException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(user.getId(), id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateById(user.getId(), id, name, description);
        if (projectUpdated == null) incorrectValue();
    }

}
