package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.command.ProjectAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

public class ProjectCreateCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() throws ProjectNotFoundException, EmptyNameException, EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter name");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        String description = TerminalUtil.nextLine();
        final Project project = add(name, description);
        serviceLocator.getProjectService().add(user.getId(), project);
    }

}
