package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.command.ProjectAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.User;

public class ProjectClearCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    public void execute() throws EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        serviceLocator.getProjectService().clear(user.getId());
    }
}
