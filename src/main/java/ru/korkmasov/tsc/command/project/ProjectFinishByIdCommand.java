package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.command.ProjectAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

public class ProjectFinishByIdCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by id.";
    }

    @Override
    public void execute() throws EmptyIdException, ProjectNotFoundException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter id");
        String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishById(user.getId(), id);
        if (project == null) throw new ProjectNotFoundException();
    }

}
