package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.command.ProjectAbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class ProjectUpdateByIndexCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() throws IndexIncorrectException, EmptyIndexException, ProjectNotFoundException, EmptyNameException, EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().findByIndex(user.getId(), index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateByIndex(user.getId(), index, name, description);
        if (projectUpdated == null) incorrectValue();
    }

}
