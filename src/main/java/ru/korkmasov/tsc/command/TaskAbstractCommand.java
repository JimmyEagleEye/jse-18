package ru.korkmasov.tsc.command;

import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;

public abstract class TaskAbstractCommand extends AbstractCommand {

    protected void showTask(final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project Id: " + task.getProjectId());
    }

    protected Task add(final String name, final String description) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Task(name, description);
    }

}
