package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.toString());
        }
    }
}
