package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.NumberUtil;

public class InfoDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "info";
    }

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Display system info.";
    }

    @Override
    public void execute() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }
}

