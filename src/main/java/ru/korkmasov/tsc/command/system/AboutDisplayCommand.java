package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

public class AboutDisplayCommand extends AbstractCommand {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("Aleksey Pisarev");
        System.out.println("pisarevaleks@mail.ru");
    }

}
