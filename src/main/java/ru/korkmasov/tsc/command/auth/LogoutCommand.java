package ru.korkmasov.tsc.command.auth;

import ru.korkmasov.tsc.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout from to application.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }
}
