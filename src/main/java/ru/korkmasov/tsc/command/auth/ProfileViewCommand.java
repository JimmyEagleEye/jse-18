package ru.korkmasov.tsc.command.auth;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.User;

public class ProfileViewCommand extends AbstractCommand {
    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View profile.";
    }

    @Override
    public void execute() throws EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }
}
