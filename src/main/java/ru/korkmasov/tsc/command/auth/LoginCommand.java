package ru.korkmasov.tsc.command.auth;

import ru.korkmasov.tsc.command.AuthAbstractCommand;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logging in to application.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
