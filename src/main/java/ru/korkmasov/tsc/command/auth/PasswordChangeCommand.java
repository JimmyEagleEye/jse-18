package ru.korkmasov.tsc.command.auth;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

public class PasswordChangeCommand extends AbstractCommand {
    @Override
    public String name() {
        return "password-change";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change password.";
    }

    @Override
    public void execute() throws EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter new password");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(user.getId(), password);
    }
}
