package ru.korkmasov.tsc.command.auth;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

public class ProfileUpdateCommand extends AbstractCommand {
    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    public void execute() throws EmptyIdException {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("Enter first name");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(user.getId(), firstName, lastName, middleName);
    }
}
