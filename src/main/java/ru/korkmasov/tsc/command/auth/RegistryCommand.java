package ru.korkmasov.tsc.command.auth;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.TerminalUtil;

public class RegistryCommand extends AbstractCommand {
    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }
}
