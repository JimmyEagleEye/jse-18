package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;


import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String userId, final String projectId) throws ProjectNotFoundException;

    Task bindTaskById(String userId, final String taskId, final String projectId) throws ProjectNotFoundException, TaskNotFoundException;

    Task unbindTaskById(String userId, final String taskId) throws TaskNotFoundException;

    Project removeProjectById(String userId, final String projectId) throws ProjectNotFoundException;

    Project removeProjectById(final String projectId) throws ProjectNotFoundException;

    Project removeProjectByIndex(String userId, final Integer index);

    Project removeProjectByName(String userId, final String name);

}
