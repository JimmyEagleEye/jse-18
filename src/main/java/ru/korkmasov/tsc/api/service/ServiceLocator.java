package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.ICommandService;
import ru.korkmasov.tsc.api.IProjectService;
import ru.korkmasov.tsc.api.IProjectTaskService;
import ru.korkmasov.tsc.api.ITaskService;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

}
