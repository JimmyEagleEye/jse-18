package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.User;

public interface IAuthService {

    String getUserId();

    User getUser() throws EmptyIdException;

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
