package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User findById(String id) throws EmptyIdException;

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    User removeById(String id) throws EmptyIdException;

    User removeByLogin(String login);

    User add(String login, String password);

    User add(String login, String password, String email);

    User setPassword(String id, String password) throws EmptyIdException;

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    ) throws EmptyIdException;
}
