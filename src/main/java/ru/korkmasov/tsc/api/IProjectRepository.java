package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.User;

import java.util.List;
import java.util.Comparator;

public interface IProjectRepository {

    List<Project> findAll(String userId);

    List<Project> findAll();

    List<Project> findAll(final String userId, final Comparator<Project> comparator);

    Project findById(final String userId, final String id);

    Project findByName(String name);

    Project findByName(final String userId, final String name);

    void add(final String userId, final Project project);

    void remove(final String userId, final Project project);

    Project removeById(final String userId, final String id);

    Project removeByName(final String userId, final String name);

    Project removeByIndex(final String userId, final int index);

    void clear(final String userId);

    Project findByIndex(final String userId, final int index);
}
