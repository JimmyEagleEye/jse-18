package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;


import java.util.List;
import java.util.Comparator;

public interface IProjectService {

    List<Project> findAll(final String userId);

    List<Project> findAll(final String userId, final Comparator<Project> comparator);

    Project add(String name, String description) throws EmptyNameException;

    Project findById(final String userId, final String id) throws EmptyIdException;

    Project findByName(final String userId, final String name) throws EmptyNameException;

    Project findByIndex(final String userId, final Integer index) throws EmptyIndexException;

    Project add(final String userId, final Project project) throws ProjectNotFoundException;

    void remove(final String userId, final Project project) throws ProjectNotFoundException;

    Project removeById(final String userId, final String id) throws EmptyIdException;

    Project removeByName(final String userId, final String name) throws EmptyNameException;

    Project removeByIndex(final String userId, final Integer index) throws EmptyIndexException;

    void clear(final String userId);

    Project updateById(final String userId, final String id, final String name, final String description) throws EmptyNameException, EmptyIdException, ProjectNotFoundException;

    Project updateByIndex(final String userId, final Integer index, final String name, final String description) throws EmptyNameException, EmptyIndexException, ProjectNotFoundException;

    Project startById(final String userId, final String id) throws EmptyIdException, ProjectNotFoundException;

    Project startByIndex(final String userId, final Integer index) throws EmptyIndexException, ProjectNotFoundException;

    Project startByName(final String userId, final String name) throws EmptyNameException, ProjectNotFoundException;

    Project finishById(final String userId, final String id) throws EmptyIdException, ProjectNotFoundException;

    Project finishByIndex(final String userId, final Integer index) throws EmptyIndexException, ProjectNotFoundException;

    Project finishByName(final String userId, final String name) throws EmptyNameException, ProjectNotFoundException;

}
