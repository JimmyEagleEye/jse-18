package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.service.CommandService;
import ru.korkmasov.tsc.command.AbstractCommand;

import java.util.Collection;


public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getListCommandName();

    Collection<String> getListCommandArg();

    void add(AbstractCommand command);

}
