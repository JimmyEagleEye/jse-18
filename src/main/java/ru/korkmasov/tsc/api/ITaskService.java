package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;

import java.util.List;
import java.util.Comparator;

public interface ITaskService {

    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    Task findById(String userId, String id) throws EmptyIdException;

    Task findByName(String userId, String name) throws EmptyNameException;

    Task findByIndex(String userId, Integer index) throws EmptyIndexException;

    Task add(String name, String description) throws EmptyNameException;

    Task add(String userId, Task task) throws TaskNotFoundException;

    void remove(String userId, Task task) throws TaskNotFoundException;

    Task removeById(String userId, String id) throws EmptyIdException;

    Task removeByName(String userId, String name) throws EmptyNameException;

    Task removeByIndex(String userId, Integer index) throws EmptyIndexException;

    void clear(String userId);

    Task updateById(String userId, final String id, final String name, final String description) throws EmptyIdException, TaskNotFoundException, EmptyNameException;

    Task updateByIndex(String userId, final Integer index, final String name, final String description) throws EmptyIndexException, TaskNotFoundException, EmptyNameException;

    Task startById(String userId, String id) throws EmptyIdException, TaskNotFoundException;

    Task startByIndex(String userId, Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task startByName(String userId, String name) throws TaskNotFoundException, EmptyNameException;

    Task finishById(String userId, String id) throws EmptyIdException, TaskNotFoundException;

    Task finishByIndex(String userId, Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task finishByName(String userId, String name) throws TaskNotFoundException, EmptyNameException;

}
