package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.User;

import java.util.List;

public interface IUserRepository {
    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUser(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);
}
