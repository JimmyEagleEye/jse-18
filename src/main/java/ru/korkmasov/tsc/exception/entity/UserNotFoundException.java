package ru.korkmasov.tsc.exception.entity;

import ru.korkmasov.tsc.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
