package ru.korkmasov.tsc.exception.entity;

import ru.korkmasov.tsc.exception.AbstractException;

public class LoginExistException extends AbstractException {

    public LoginExistException() {
        super("Error. Login already exist.");
    }

    public LoginExistException(String value) {
        super("Error. Login '"+value+"' already exist.");
    }

}
