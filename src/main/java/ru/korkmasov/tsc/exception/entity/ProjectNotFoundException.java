package ru.korkmasov.tsc.exception.entity;

public class ProjectNotFoundException extends Exception {

    public ProjectNotFoundException() {
        super("Error! Project not found!");
    }

}
