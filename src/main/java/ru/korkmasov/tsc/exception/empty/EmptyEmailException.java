package ru.korkmasov.tsc.exception.empty;

import ru.korkmasov.tsc.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty");
    }

}
