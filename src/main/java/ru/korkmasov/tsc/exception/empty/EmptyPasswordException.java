package ru.korkmasov.tsc.exception.empty;

import ru.korkmasov.tsc.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error. Password is empty");
    }

}
