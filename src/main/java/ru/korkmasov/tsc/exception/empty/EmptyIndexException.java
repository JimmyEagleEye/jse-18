package ru.korkmasov.tsc.exception.empty;

public class EmptyIndexException extends Exception {

    public EmptyIndexException() {
        super("Error! Index is empty...");
    }

}
