package ru.korkmasov.tsc.exception.empty;

import ru.korkmasov.tsc.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty");
    }

}
