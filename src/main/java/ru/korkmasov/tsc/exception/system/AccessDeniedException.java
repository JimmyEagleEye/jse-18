package ru.korkmasov.tsc.exception.system;

import ru.korkmasov.tsc.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
