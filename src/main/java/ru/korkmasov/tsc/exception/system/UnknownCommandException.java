package ru.korkmasov.tsc.exception.system;

public class UnknownCommandException extends Exception {

    public UnknownCommandException(String command) {
        super("Error! Unknown command " + command);
    }

}
