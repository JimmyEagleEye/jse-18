package ru.korkmasov.tsc.util;

import java.util.Scanner;

import ru.korkmasov.tsc.exception.system.IndexIncorrectException;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static void displayWait() {
        System.out.println();
        System.out.println("Enter command.");
    }

    static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    static Integer nextNumber() throws IndexIncorrectException {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

    static void incorrectValue() {
        System.out.println("Incorrect values");
    }

}
