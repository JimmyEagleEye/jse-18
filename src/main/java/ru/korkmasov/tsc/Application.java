package ru.korkmasov.tsc;

import ru.korkmasov.tsc.bootstrap.Bootstrap;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.exception.system.UnknownArgumentException;
import ru.korkmasov.tsc.exception.system.UnknownCommandException;

public class Application {

    public static void main(final String[] args) throws UnknownArgumentException, EmptyNameException, IndexIncorrectException, ProjectNotFoundException, EmptyIdException, TaskNotFoundException, EmptyIndexException, UnknownCommandException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
