package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.ITaskRepository;
import ru.korkmasov.tsc.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll(final String userId) {
        List<Task> list = new ArrayList<>();
        for (Task task : this.list) {
            if (userId.equals(task.getUserId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(findAll(userId));
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getId())) listByProject.add(task);
        }
        return listByProject;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        list.removeAll(listByProject);
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String taskId, final String projectId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, final String id) {
        final Task task = findById(userId, id);
        task.setProjectId("");
        return task;
    }

    @Override
    public Task findById(final String userId, final String id) {
        for (Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final String userId, final int index) {
        List<Task> list = findAll(userId);
        return list.get(index);
    }

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        list.add(task);
    }

    @Override
    public void remove(String userId, Task task) {

    }

    @Override
    public Task removeById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear(final String userId) {
        List<Task> list = findAll(userId);
        this.list.removeAll(list);
    }

}
