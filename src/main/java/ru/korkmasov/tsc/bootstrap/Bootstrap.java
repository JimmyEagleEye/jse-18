package ru.korkmasov.tsc.bootstrap;

import ru.korkmasov.tsc.api.*;
import ru.korkmasov.tsc.api.repository.IUserRepository;
import ru.korkmasov.tsc.api.service.IAuthService;
import ru.korkmasov.tsc.api.service.IUserService;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.repository.*;
import ru.korkmasov.tsc.service.*;
import ru.korkmasov.tsc.command.auth.*;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.constant.ArgumentConst;
import ru.korkmasov.tsc.constant.TerminalConst;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.exception.system.UnknownArgumentException;
import ru.korkmasov.tsc.exception.system.UnknownCommandException;
import ru.korkmasov.tsc.enumerated.Role;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.command.project.*;
import ru.korkmasov.tsc.command.system.*;
import ru.korkmasov.tsc.command.task.*;
import ru.korkmasov.tsc.service.*;


import java.util.Scanner;

import static ru.korkmasov.tsc.util.TerminalUtil.displayWait;
import static ru.korkmasov.tsc.util.TerminalUtil.displayWelcome;

public final class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);
    private final ILogService logService = new LogService();

    public void run(final String[] args) throws UnknownArgumentException, EmptyNameException, UnknownCommandException, ProjectNotFoundException, TaskNotFoundException, EmptyIdException, IndexIncorrectException, EmptyIndexException {
        //initData();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ArgumentsDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskBindToProjectByIdCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFindAllByProjectIdCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindTaskByIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new LoginCommand());
        registry(new LogoutCommand());
        registry(new PasswordChangeCommand());
        registry(new ProfileUpdateCommand());
        registry(new ProfileViewCommand());
        registry(new RegistryCommand());
    }

    /*private void initData() throws EmptyNameException
    {
        projectService.add("Project 1", "-").setStatus(Status.NOT_STARTED);
        projectService.add("Project 2", "-").setStatus(Status.IN_PROGRESS);
        projectService.add("Project 3", "-").setStatus(Status.COMPLETED);
        projectService.add("Project 4", "-").setStatus(Status.COMPLETED);
    }*/ {
        String adminId = userService.add("admin", "admin", "admin@a").setRole(Role.ADMIN).getId();
        userService.add("user", "user");
        try {
            projectService.add(adminId, new Project("Project A", "-"));
            projectService.add(adminId, new Project("Project B", "-")).setStatus(Status.IN_PROGRESS);
            projectService.add(adminId, new Project("Project C", "-")).setStatus(Status.COMPLETED);
            projectService.add(adminId, new Project("Project D", "-")).setStatus(Status.COMPLETED);
            taskService.add(adminId, new Task("Task A", "-"));
            taskService.add(adminId, new Task("Task B", "-")).setStatus(Status.IN_PROGRESS);
            taskService.add(adminId, new Task("Task C", "-")).setStatus(Status.COMPLETED);
            taskService.add(adminId, new Task("Task D", "-")).setStatus(Status.COMPLETED);
        } catch (ProjectNotFoundException | TaskNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean parseArgs(String[] args) throws UnknownArgumentException, UnknownCommandException, IndexIncorrectException, EmptyNameException, ProjectNotFoundException, EmptyIdException, TaskNotFoundException, EmptyIndexException {
        if (args == null || args.length == 0) return false;
        if (args.length < 1) return false;
        //final String arg = args[0];
        //commandRepository.getCommandByArg(args[0]).execute();
        AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();

        return true;
    }

    public void parseCommand(final String command) throws IndexIncorrectException, TaskNotFoundException, ProjectNotFoundException, UnknownCommandException, EmptyIdException, EmptyNameException, EmptyIndexException {
        if (command == null) return;
        //commandRepository.getCommandByName(command).execute();
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();

    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);

    }

    public void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

    public void process() {
        final Scanner scanner = new Scanner(System.in);
        logService.debug("Testing mode.");
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                //System.out.println();
                //System.out.println("Enter command.");
                command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Completed");
                System.err.println("[OK]");
            } catch (Exception e) {
                logService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
